//
//  KKConstants.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 10.12.12.
//
//

#import "KKConstants.h"

@implementation KKConstants

NSString* const UserDidTouchWindowNotification = @"UserDidTouchWindow";
NSString* const TimerDidEndNotification = @"TimerDidEnd";
NSString* const ApplicationShouldHideLanguageMenu = @"ApplicationShouldHideLanguageMenu";

NSString* const ApplicationFont = @"Helvetica";

@end
