//
//  KKNavigationViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/11/13.
//
//

#import <UIKit/UIKit.h>

@interface KKNavigationViewController : UIViewController

@property (nonatomic) NSArray *presentations;
@property (assign, nonatomic) BOOL shouldEnterRestmode;
@property (weak, nonatomic) IBOutlet UIView *buttonContainer;
@property (nonatomic, strong) UIViewController	 *activeViewController;


@end
