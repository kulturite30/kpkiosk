//
//  KKConstants.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 10.12.12.
//
//

#import <Foundation/Foundation.h>

@interface KKConstants : NSObject


extern NSString* const UserDidTouchWindowNotification;
extern NSString* const TimerDidEndNotification;
extern NSString* const ApplicationShouldHideLanguageMenu;


extern NSString* const ApplicationFont;


@end
