//
//  KKGalleryViewController.h
//  Kulturkiosk
//
//  Created by Rune Botten on 04.10.12.
//
//

#import <UIKit/UIKit.h>
#import "KKResourceCreditView.h"

@interface KKGalleryViewController : UIViewController
@property (retain, nonatomic) NSArray *resources;
@property (retain, nonatomic) NSArray *resourceDetails;
@property (strong, nonatomic) NSMutableArray *thumbViews;
@property (strong, nonatomic) KKResourceCreditView *infoView;
@property (assign) NSUInteger currentIndex;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (NSInteger)indexOfItemView:(UIView *)view;
- (void)didTap:(UITapGestureRecognizer *)tapGesture;
-(void) selectThumb:(NSInteger) index;
-(void) showInfo:(NSUInteger)index;
-(void) hideInfo;

@end
