//
//  KKPresentationViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/16/13.
//
//

#import <UIKit/UIKit.h>

@interface KKPresentationViewController : UIViewController

@property (strong, nonatomic) Presentation *presentation;

- (void)highlightNode:(Nodes*)node;

@end
