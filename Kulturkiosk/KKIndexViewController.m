//
//  KKIndexViewController.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 01.08.13.
//
//

#import "KKIndexViewController.h"
#import "KKIndexCollectionViewCell.h"
#import "KKCardLayout.h"

@interface KKIndexViewController ()
- (void)languageChange;

@end

@implementation KKIndexViewController


- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
  NSArray *records = [self.presentation.records allObjects];
  self.sortedRecords = [records sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
    Content *c1 = [[(Record*)obj1 node] contentForLanguage:[KKLanguage currentLanguage]];
    Content *c2 = [[(Record*)obj2 node] contentForLanguage:[KKLanguage currentLanguage]];
    return [c1.title caseInsensitiveCompare:c2.title];
  }];
    
  self.pagerControl.numberOfPages=ceil((double)[self.presentation.records count]/6);
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(languageChange)
                                               name:@"languageChange"
                                             object:nil];

}


- (void)languageChange
{
  [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)highlightContentInMapPresentation:(id)sender {
  
  UIButton *button = (UIButton*) sender;
  
  Nodes *node = [Nodes objectWithPredicate:[NSPredicate predicateWithFormat:@"item_id == %@", [NSNumber numberWithInt:button.tag]]];
  Presentation *presentation = [Presentation objectWithPredicate:[NSPredicate predicateWithFormat:@"type == 'map' AND ANY records.node == %@", node]];
  
  NSDictionary *userinfo = @{@"presentationId":presentation.identifier, @"nodeId":node.item_id};
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"highlightContentInPresentation" object:nil userInfo:userinfo];

}

- (IBAction)highlightContentInTimeLinePresentation:(id)sender {
  UIButton *button = (UIButton*) sender;
  
  Nodes *node = [Nodes objectWithPredicate:[NSPredicate predicateWithFormat:@"item_id == %@", [NSNumber numberWithInt:button.tag]]];
  Presentation *presentation = [Presentation objectWithPredicate:[NSPredicate predicateWithFormat:@"type == 'timeline' AND ANY records.node == %@", node]];
  
  NSDictionary *userinfo = @{@"presentationId":presentation.identifier, @"nodeId":node.item_id};
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"highlightContentInPresentation" object:nil userInfo:userinfo];
}

#pragma mark - Collection View Data Sources

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  if(self.presentation) {
    return [self.presentation.records count];
  }
  return 0;
}

// The cell that is returned must be retrieved from a call to - dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (KKIndexCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKIndexCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"indexViewCell" forIndexPath:indexPath];
  [cell populateWithRecord: [self.sortedRecords objectAtIndex:indexPath.row]];
  
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  Record *record = [self.sortedRecords objectAtIndex:indexPath.row];
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"didTapCallout" object:nil userInfo:@{@"identifier": record.node.item_id}];
}

- (void)highlightNode:(Nodes *)node
{
  for (NSUInteger i = 0; i < [self.sortedRecords count]; i++) {
    Record *record = self.sortedRecords[i];
    if ([record.node.item_id intValue] == [node.item_id intValue]) {
      NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForItem:(NSInteger)i inSection:0];
      [self.collectionView scrollToItemAtIndexPath:selectedIndexPath
                                  atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                          animated:YES];
      break;
    }
  }
}

- (NSInteger)horizontalPageNumber:(UIScrollView *)scrollView {
  CGPoint contentOffset = scrollView.contentOffset;
  CGSize viewSize = scrollView.bounds.size;
  
  NSInteger horizontalPage = MAX(0.0, (float)contentOffset.x / viewSize.width);
  
  return horizontalPage;
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
  self.pagerControl.currentPage = [self horizontalPageNumber:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
  if (!decelerate) self.pagerControl.currentPage = [self horizontalPageNumber:scrollView];
}


@end
