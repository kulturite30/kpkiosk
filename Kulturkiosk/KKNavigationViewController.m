//
//  KKNavigationViewController.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/11/13.
//
//

#import "KKNavigationViewController.h"
#import "KKContainerViewController.h"
#import "KKHelpViewController.h"
#import "KKMapViewController.h"
#import "KKIndexViewController.h"
#import "KKContainerViewController.h"
#import "KKTimeLineViewController.h"
#import "KKPresentationViewController.h"
#import "KKPopoverBackgroundView.h"
#import "KKVideoGalleryViewController.h"
#import "KKImageGalleryViewController.h"
#import "KKTextContentViewController.h"


@interface KKNavigationViewController ()

@property (nonatomic, strong) NSMutableDictionary *viewControllers;
@property (weak, nonatomic) IBOutlet UIView *activeView;
@property (nonatomic, weak) UIPopoverController *presentedPopoverController;
@property (assign, nonatomic) BOOL restmode;
@property (assign, nonatomic) BOOL isDisplayingNodeContent;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (assign, nonatomic) KKHelpType helpType;
@property (weak, nonatomic) IBOutlet UIView *navigationBar;

@end

@implementation KKNavigationViewController

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLoad
{
  [super viewDidLoad];
  
  self.shouldEnterRestmode = YES;
  
  self.navigationBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar-gradient.png"]];
  
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didTapCallout:) name:@"didTapCallout" object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeLanguage) name:@"languageChange" object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(highlightContentInPresentation:) name:@"highlightContentInPresentation" object:nil];
    
  Device *device=[Device findFirst];
  self.presentations=[device.presentations sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
  self.restmode = device.idleScreenActiveValue;
  self.homeButton.hidden=!self.restmode;
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetViewState) name:TimerDidEndNotification object:nil];
  
  self.viewControllers = [NSMutableDictionary dictionaryWithCapacity:[self.presentations count]];
  
  [self.buttonContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
  
  int buttonCount = 0;
  NSMutableDictionary *buttons = [NSMutableDictionary dictionary];
  
  for (Presentation *presentation in self.presentations) {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", presentation.type]] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-active.png", presentation.type]] forState:UIControlStateHighlighted];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-active.png", presentation.type]] forState:UIControlStateSelected];
    [button addTarget:self action:NSSelectorFromString([NSString stringWithFormat:@"show%@View:",[presentation className]]) forControlEvents:UIControlEventTouchUpInside];
    [button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [button setTag:presentation.identifierValue];
    [buttons setObject:button forKey:[NSString stringWithFormat:@"button%d", buttonCount]];
    [self.buttonContainer addSubview:button];
    
    buttonCount++;
  }
}

-(void)viewDidAppear:(BOOL)animated
{
  [UIAppDelegate startTimer];
  [super viewDidAppear:animated];
  if (self.presentations && !self.activeViewController)
  {
      // Tap the first button in the presentations-container
    UIButton *button = [[self.buttonContainer subviews] objectAtIndex:0];
    [button sendActionsForControlEvents:UIControlEventTouchUpInside];
  }
}

- (void)viewDidLayoutSubviews
{
  [super viewDidLayoutSubviews];
  NSMutableDictionary *buttons = [NSMutableDictionary dictionary];
  int buttonCount = 0;
  
  for (UIView *view in self.buttonContainer.subviews) {
    [buttons setObject:view forKey:[NSString stringWithFormat:@"item%d", buttonCount]];
    buttonCount++;
  }
  
  NSMutableString *constraintString = [NSMutableString string];

  NSUInteger edgeSpacing = (540 - ((buttonCount - 1) * 30) - (buttonCount * 40)) / 2;
  
  for (int i = 0; i < self.buttonContainer.subviews.count; i++) {
    
    if (i == 0) {
      [constraintString appendFormat:@"|-%d-[%@(40)]", edgeSpacing, [buttons allKeysForObject:self.buttonContainer.subviews[i]][0]];
    } else {
      [constraintString appendFormat:@"-60-[%@(40)]", [buttons allKeysForObject:self.buttonContainer.subviews[i]][0]];
    }
    NSLayoutConstraint *verticalConstraint = [NSLayoutConstraint constraintWithItem:self.buttonContainer.subviews[i]
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.buttonContainer
                                                                          attribute:NSLayoutAttributeTop
                                                                         multiplier:0
                                                                           constant:0];
    [self.buttonContainer addConstraint:verticalConstraint];
  }
  if(self.buttonContainer.subviews.count) {
    [self.buttonContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraintString options:NSLayoutFormatAlignAllCenterY metrics:nil views:buttons]];
    [self.buttonContainer setNeedsLayout];
  }
}

- (void)highlightContentInPresentation:(id)notification
{

  float delay = 0.2;
 if(![[self view] window])
   return;
  NSNumber *nodeId = [notification valueForKeyPath:@"userInfo.nodeId"];
  Nodes *node = [Nodes objectWithPredicate:[NSPredicate predicateWithFormat:@"item_id == %@",nodeId]];
  NSNumber *presentationId = [notification valueForKeyPath:@"userInfo.presentationId"];
  Presentation *presentation = [self presentationWithIdentifier:[presentationId intValue]];
  UIButton *button = nil;
  for (UIButton *_button in self.buttonContainer.subviews) {
    if (_button.tag == [presentationId integerValue]) {
      button = _button;
      break;
    }
  }
  
  if ([self.activeViewController isKindOfClass:[KKMapViewController class]]) {
    KKMapViewController *mapController = (KKMapViewController*) self.activeViewController;
    [mapController viewWillDisappear:NO];
  }
  
  if ([presentation.type isEqualToString:@"timeline"]) {
    KKTimeLineViewController *timelineController = (KKTimeLineViewController*) [self viewcontrollerForPresentationType:presentation.type];
    [timelineController restoreScreen];
    delay = 0.7;
  }
  
  [self performSelector:@selector(showPresentationWithSender:) withObject:button];
  
  [self.activeViewController performSelector:@selector(highlightNode:) withObject:node afterDelay:delay];
  
}

- (void)resetViewState
{
  [self.presentedPopoverController dismissPopoverAnimated:NO];
  self.presentedPopoverController = nil;
  
  if ([self.childViewControllers count] && [self.childViewControllers[0] isKindOfClass:[KKHelpViewController class]]) {
      [[self.childViewControllers[0] view] removeFromSuperview];
      [self.childViewControllers[0] removeFromParentViewController];
  }
  
  if ([self.activeViewController isKindOfClass:[KKMapViewController class]]) {
    KKMapViewController *mapController = (KKMapViewController*) self.activeViewController;
    [mapController.presentingPopover dismissPopoverAnimated:NO];
    mapController.presentingPopover = nil;
  }
  
  if ([self.activeViewController.presentedViewController isKindOfClass:[KKImageGalleryViewController class]]
      || [self.activeViewController.presentedViewController isKindOfClass:[KKTextContentViewController class]]
      || [self.activeViewController.presentedViewController  isKindOfClass:[KKVideoGalleryViewController class]]) {
    
    if ([self.activeViewController.presentedViewController isKindOfClass:[KKVideoGalleryViewController class]]) {
      KKVideoGalleryViewController *videoGalleryController = (KKVideoGalleryViewController*)self.activeViewController.presentedViewController;
      if (videoGalleryController.moviePlayer.fullscreen) {
        videoGalleryController.moviePlayer.fullscreen = NO;
      }
    }
    
    [self.activeViewController dismissViewControllerAnimated:YES completion:^{
      [self performViewStateResetCompletion];
    }];
    
  }else{
    [self performViewStateResetCompletion];
  }

}

- (void)performViewStateResetCompletion
{
  Device *device=[Device findFirst];
  
  for (UIView *subview in self.activeView.subviews) {
    [subview removeFromSuperview];
  };  self.activeViewController=nil;
  self.viewControllers = [NSMutableDictionary dictionaryWithCapacity:[self.presentations count]];  
  if (self.shouldEnterRestmode && self.restmode)
  {
    if (self.view.window != nil) {
      UIStoryboard *storyboard = [UIStoryboard storyboardWithName: @"MainStoryboard" bundle:nil];
      
      UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier: @"HomeScreen"];
      [self presentViewController:viewController  animated:YES completion:^{
        NSLog(@"Look ma, no hands");
      }];

    }
   
  }else if (device.defaultPresentation){
    
    for (UIButton *button in self.buttonContainer.subviews) {
      if (button.tag == device.defaultPresentationValue) {
        [button sendActionsForControlEvents:UIControlEventTouchUpInside];
        break;
      }
    }
    
  }else {
    UIButton *button = [self.buttonContainer.subviews objectAtIndex:0];
    [button sendActionsForControlEvents:UIControlEventTouchUpInside];
  }
}


#pragma mark Segue actions

- (void)removeSelectedStateFromButtons
{
  for (UIButton *button in self.buttonContainer.subviews) {
    [button setSelected:NO];
  }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

  if ([segue.identifier isEqualToString:@"DisplayHelpView"]) {
    KKHelpViewController *vc = segue.destinationViewController;
    [vc setHelpType:self.helpType];
  }
  if ([segue.identifier isEqualToString:@"DisplayLanguageMenu"]) {
    self.presentedPopoverController = [(UIStoryboardPopoverSegue*)segue popoverController];
    self.presentedPopoverController.popoverBackgroundViewClass = [KKPopoverBackgroundView class];
    
    CGFloat popoverHeight = ([[KKLanguage availableLanguages] count] > 3 ? 200 : 100);
    self.presentedPopoverController.popoverContentSize = CGSizeMake(630, popoverHeight);
  }
}

- (Presentation *) presentationWithIdentifier:(int)index
{
  for (Presentation *presentation in self.presentations) {
    if (presentation.identifierValue == index) {
      return presentation;
    }
  }
  return nil;
}

- (UIViewController*) viewcontrollerForPresentationType:(NSString*)presentationType
{
  
  NSString *type = [presentationType lowercaseString];
  
  UIViewController *viewController;

  if ([self.viewControllers objectForKey:type]) {
    viewController = [self.viewControllers objectForKey:type];
  }
  else
  {
  
    if ([type isEqualToString:@"single"])
    {
      viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"SingleViewController"];
    }
    else if ([type isEqualToString:@"map"])
    {
      viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    }
    else if ([type isEqualToString:@"timeline"])
    {
      viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"TimelineViewController"];
    }
    else if ([type isEqualToString:@"index"])
    {
      viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"IndexViewController"];
    }
    
    else if ([type isEqualToString:@"image_gallery"] || [type isEqualToString:@"video_gallery"])
    {
      viewController = [MainStoryboard instantiateViewControllerWithIdentifier:@"GalleryListViewController"];
    }
    [self.viewControllers setValue:viewController forKey:type];
    
  }
  
  return viewController;
}

- (void)showPresentationWithSender:(id)sender
{
  [self removeSelectedStateFromButtons];
  
  UIButton *button = (UIButton*) sender;
  
  [button setSelected:YES];
  Presentation *presentation = [self presentationWithIdentifier:button.tag];
  
  UIViewController *viewController = [self viewcontrollerForPresentationType:presentation.type];
  if ([viewController respondsToSelector:@selector(setPresentation:)]) {
    [viewController performSelector:@selector(setPresentation:) withObject:presentation];
  }
  if (self.activeViewController == viewController)
    return;
  
  for (UIView *subview in self.activeView.subviews) {
    [subview removeFromSuperview];
  }

  self.activeViewController = viewController;
  self.activeViewController.view.frame=self.activeView.frame;
  [self.activeView addSubview:self.activeViewController.view];
}

- (void)displayNode:(Nodes*)node
{
  
  KKContainerViewController *vc = (KKContainerViewController*) [self viewcontrollerForPresentationType:@"single"];
  vc.presentation = nil;
  vc.node = node;
  
  self.activeViewController = vc;
  self.activeViewController.view.frame=self.activeView.frame;
  [self.activeView addSubview:self.activeViewController.view];
  
}

- (void)showTimelineView:(id)sender
{
  self.helpType = KKHelpTypeSwipe;
  [self showPresentationWithSender:sender];
}

- (void)showMapView:(id)sender {
  self.helpType = KKHelpTypeZoom;
  [self showPresentationWithSender:sender];
}

- (void)showSingleView:(id)sender
{
  self.helpType = KKHelpTypeTap;
  [self showPresentationWithSender:sender];
}

- (void)showIndexView:(id)sender
{
  self.helpType = KKHelpTypeSwipe;
  [self showPresentationWithSender:sender];
}

- (void)showVideoGalleryView:(id)sender
{
  self.helpType = KKHelpTypeSwipe;
  [self showPresentationWithSender:sender];
}

- (void)showImageGalleryView:(id)sender
{
  self.helpType = KKHelpTypeSwipe;
  [self showPresentationWithSender:sender];
}



- (IBAction)enterRestMode:(id)sender
{
  self.shouldEnterRestmode = YES;
  [self resetViewState];
}
- (IBAction)goBack:(id)sender
{
  
}
- (IBAction)goForward:(id)sender
{
  
}

- (void)didTapCallout:(id)notification
{
  NSNumber *identifier = [notification valueForKeyPath:@"userInfo.identifier"];
  Nodes *node = [Nodes objectWithPredicate:[NSPredicate predicateWithFormat:@"item_id == %@", identifier]];
  [self.activeViewController viewWillDisappear: NO];
  [self displayNode:node];
}

- (void)didChangeLanguage
{
  [self.presentedPopoverController dismissPopoverAnimated:YES];
}

- (IBAction)showHelpForCurrentPresentation:(id)sender
{
  
}
@end
