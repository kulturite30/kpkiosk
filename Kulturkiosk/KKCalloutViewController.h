//
//  KKCalloutViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 6/14/13.
//
//

#import <UIKit/UIKit.h>

@class KKAnnotation;

@interface KKCalloutViewController : UIViewController

- (void)setAnnotation:(KKAnnotation *)annotation;

@end
