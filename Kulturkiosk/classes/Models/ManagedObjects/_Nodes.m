// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Nodes.m instead.

#import "_Nodes.h"

const struct NodesAttributes NodesAttributes = {
	.identifier = @"identifier",
	.item_id = @"item_id",
	.node_id = @"node_id",
	.page_id = @"page_id",
	.parent = @"parent",
	.position = @"position",
	.record_id = @"record_id",
	.title = @"title",
	.type = @"type",
};

const struct NodesRelationships NodesRelationships = {
	.children = @"children",
	.content = @"content",
	.parent_id = @"parent_id",
	.records = @"records",
	.resources = @"resources",
};

const struct NodesFetchedProperties NodesFetchedProperties = {
};

@implementation NodesID
@end

@implementation _Nodes

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Nodes" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Nodes";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Nodes" inManagedObjectContext:moc_];
}

- (NodesID*)objectID {
	return (NodesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"item_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"item_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"node_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"node_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"page_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"page_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"parentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"parent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"record_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"record_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic identifier;






@dynamic item_id;



- (int16_t)item_idValue {
	NSNumber *result = [self item_id];
	return [result shortValue];
}

- (void)setItem_idValue:(int16_t)value_ {
	[self setItem_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveItem_idValue {
	NSNumber *result = [self primitiveItem_id];
	return [result shortValue];
}

- (void)setPrimitiveItem_idValue:(int16_t)value_ {
	[self setPrimitiveItem_id:[NSNumber numberWithShort:value_]];
}





@dynamic node_id;



- (int16_t)node_idValue {
	NSNumber *result = [self node_id];
	return [result shortValue];
}

- (void)setNode_idValue:(int16_t)value_ {
	[self setNode_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNode_idValue {
	NSNumber *result = [self primitiveNode_id];
	return [result shortValue];
}

- (void)setPrimitiveNode_idValue:(int16_t)value_ {
	[self setPrimitiveNode_id:[NSNumber numberWithShort:value_]];
}





@dynamic page_id;



- (int16_t)page_idValue {
	NSNumber *result = [self page_id];
	return [result shortValue];
}

- (void)setPage_idValue:(int16_t)value_ {
	[self setPage_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePage_idValue {
	NSNumber *result = [self primitivePage_id];
	return [result shortValue];
}

- (void)setPrimitivePage_idValue:(int16_t)value_ {
	[self setPrimitivePage_id:[NSNumber numberWithShort:value_]];
}





@dynamic parent;



- (int64_t)parentValue {
	NSNumber *result = [self parent];
	return [result longLongValue];
}

- (void)setParentValue:(int64_t)value_ {
	[self setParent:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveParentValue {
	NSNumber *result = [self primitiveParent];
	return [result longLongValue];
}

- (void)setPrimitiveParentValue:(int64_t)value_ {
	[self setPrimitiveParent:[NSNumber numberWithLongLong:value_]];
}





@dynamic position;



- (int32_t)positionValue {
	NSNumber *result = [self position];
	return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
	[self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}





@dynamic record_id;



- (int16_t)record_idValue {
	NSNumber *result = [self record_id];
	return [result shortValue];
}

- (void)setRecord_idValue:(int16_t)value_ {
	[self setRecord_id:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveRecord_idValue {
	NSNumber *result = [self primitiveRecord_id];
	return [result shortValue];
}

- (void)setPrimitiveRecord_idValue:(int16_t)value_ {
	[self setPrimitiveRecord_id:[NSNumber numberWithShort:value_]];
}





@dynamic title;






@dynamic type;






@dynamic children;

	
- (NSMutableSet*)childrenSet {
	[self willAccessValueForKey:@"children"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"children"];
  
	[self didAccessValueForKey:@"children"];
	return result;
}
	

@dynamic content;

	
- (NSMutableSet*)contentSet {
	[self willAccessValueForKey:@"content"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"content"];
  
	[self didAccessValueForKey:@"content"];
	return result;
}
	

@dynamic parent_id;

	

@dynamic records;

	
- (NSMutableSet*)recordsSet {
	[self willAccessValueForKey:@"records"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"records"];
  
	[self didAccessValueForKey:@"records"];
	return result;
}
	

@dynamic resources;

	
- (NSMutableOrderedSet*)resourcesSet {
	[self willAccessValueForKey:@"resources"];
  
	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"resources"];
  
	[self didAccessValueForKey:@"resources"];
	return result;
}
	






@end
