
#import "_Nodes.h"
#import "Content.h"
#import "Files.h"
#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "KKLanguage.h"

@interface Nodes : _Nodes {}
// Custom logic goes here.

typedef NSString* KKResourceType;

FOUNDATION_EXPORT KKResourceType const KKResourceTypeImage;
FOUNDATION_EXPORT KKResourceType const KKResourceTypeVideo;

typedef enum KKType
{
    KKTypeUnknown = 0,
    KKTypeRecord,
    KKTypePage,
    KKTypeFolder
} KKType;

typedef enum KKImageFormat
{
  KKFormatHeader = 0,
  KKFormatOriginal,
  KKFormatThumb,
  KKFormatMain,
  KKFormatSmall
} KKImageFormat;

-(NSArray*) pagesForLanguage:(KKLanguageType) lang;
-(NSArray*) childrenForLanguage:(KKLanguageType) lang;
-(Content*)contentForLanguage:(KKLanguageType)lang;
-(BOOL)hasContentForLanguage:(KKLanguageType)lang;
-(BOOL) allChildrenHasContentForLanguage:(KKLanguageType) lang;
-(KKType)nodeType;
-(NSArray*)files;
-(NSArray*) resourcesWithType:(KKResourceType) type forLanguage:(KKLanguageType) lang;
-(UIImage*) backgroundImage;
-(UIImage*) mainImage;
-(UIImage*) headerImage;
-(UIImage*) thumbnail;
-(NSArray*)imagesWithType:(KKImageFormat)format;
+(NSString*)stringForImageFormat:(KKImageFormat)format;
+(Nodes*) rootNode;
-(BOOL)hasVideosForLanguage:(KKLanguageType)lang;
-(BOOL)hasImagesForLanguage:(KKLanguageType)lang;
@end
