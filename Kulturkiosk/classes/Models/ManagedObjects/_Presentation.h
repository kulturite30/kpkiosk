// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Presentation.h instead.

#import <CoreData/CoreData.h>


extern const struct PresentationAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *type;
} PresentationAttributes;

extern const struct PresentationRelationships {
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *devices;
	__unsafe_unretained NSString *records;
	__unsafe_unretained NSString *resources;
} PresentationRelationships;

extern const struct PresentationFetchedProperties {
} PresentationFetchedProperties;

@class Content;
@class Device;
@class Record;
@class Resources;






@interface PresentationID : NSManagedObjectID {}
@end

@interface _Presentation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PresentationID*)objectID;





@property (nonatomic, strong) NSNumber* identifier;



@property int16_t identifierValue;
- (int16_t)identifierValue;
- (void)setIdentifierValue:(int16_t)value_;

//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int16_t positionValue;
- (int16_t)positionValue;
- (void)setPositionValue:(int16_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *content;

- (NSMutableSet*)contentSet;




@property (nonatomic, strong) NSSet *devices;

- (NSMutableSet*)devicesSet;




@property (nonatomic, strong) NSSet *records;

- (NSMutableSet*)recordsSet;




@property (nonatomic, strong) NSSet *resources;

- (NSMutableSet*)resourcesSet;





@end

@interface _Presentation (CoreDataGeneratedAccessors)

- (void)addContent:(NSSet*)value_;
- (void)removeContent:(NSSet*)value_;
- (void)addContentObject:(Content*)value_;
- (void)removeContentObject:(Content*)value_;

- (void)addDevices:(NSSet*)value_;
- (void)removeDevices:(NSSet*)value_;
- (void)addDevicesObject:(Device*)value_;
- (void)removeDevicesObject:(Device*)value_;

- (void)addRecords:(NSSet*)value_;
- (void)removeRecords:(NSSet*)value_;
- (void)addRecordsObject:(Record*)value_;
- (void)removeRecordsObject:(Record*)value_;

- (void)addResources:(NSSet*)value_;
- (void)removeResources:(NSSet*)value_;
- (void)addResourcesObject:(Resources*)value_;
- (void)removeResourcesObject:(Resources*)value_;

@end

@interface _Presentation (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSNumber*)value;

- (int16_t)primitiveIdentifierValue;
- (void)setPrimitiveIdentifierValue:(int16_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int16_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int16_t)value_;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveContent;
- (void)setPrimitiveContent:(NSMutableSet*)value;



- (NSMutableSet*)primitiveDevices;
- (void)setPrimitiveDevices:(NSMutableSet*)value;



- (NSMutableSet*)primitiveRecords;
- (void)setPrimitiveRecords:(NSMutableSet*)value;



- (NSMutableSet*)primitiveResources;
- (void)setPrimitiveResources:(NSMutableSet*)value;


@end
