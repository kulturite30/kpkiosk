//
//  KKAssetsManager.h
//  Kulturkiosk
//
//  Created by Martin Jensen on 18.07.12.
//
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>

#import <RestKit/CoreData.h>
#import "Nodes.h"
#import "Resources.h"
#import "Content.h"
#import "Files.h"

@interface KKAssetsManager : NSObject <RKRequestDelegate, RKRequestQueueDelegate>

@property (nonatomic, strong) RKRequestQueue *queue;

+ (id)sharedManager;
-(void)loadAssetsForNode:(Nodes*)node;


@end
