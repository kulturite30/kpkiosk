// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Nodes.h instead.

#import <CoreData/CoreData.h>


extern const struct NodesAttributes {
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *item_id;
	__unsafe_unretained NSString *node_id;
	__unsafe_unretained NSString *page_id;
	__unsafe_unretained NSString *parent;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *record_id;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *type;
} NodesAttributes;

extern const struct NodesRelationships {
	__unsafe_unretained NSString *children;
	__unsafe_unretained NSString *content;
	__unsafe_unretained NSString *parent_id;
	__unsafe_unretained NSString *records;
	__unsafe_unretained NSString *resources;
} NodesRelationships;

extern const struct NodesFetchedProperties {
} NodesFetchedProperties;

@class Nodes;
@class Content;
@class Nodes;
@class Record;
@class Resources;











@interface NodesID : NSManagedObjectID {}
@end

@interface _Nodes : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (NodesID*)objectID;





@property (nonatomic, strong) NSString* identifier;



//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* item_id;



@property int16_t item_idValue;
- (int16_t)item_idValue;
- (void)setItem_idValue:(int16_t)value_;

//- (BOOL)validateItem_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* node_id;



@property int16_t node_idValue;
- (int16_t)node_idValue;
- (void)setNode_idValue:(int16_t)value_;

//- (BOOL)validateNode_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* page_id;



@property int16_t page_idValue;
- (int16_t)page_idValue;
- (void)setPage_idValue:(int16_t)value_;

//- (BOOL)validatePage_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* parent;



@property int64_t parentValue;
- (int64_t)parentValue;
- (void)setParentValue:(int64_t)value_;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* record_id;



@property int16_t record_idValue;
- (int16_t)record_idValue;
- (void)setRecord_idValue:(int16_t)value_;

//- (BOOL)validateRecord_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* type;



//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *children;

- (NSMutableSet*)childrenSet;




@property (nonatomic, strong) NSSet *content;

- (NSMutableSet*)contentSet;




@property (nonatomic, strong) Nodes *parent_id;

//- (BOOL)validateParent_id:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *records;

- (NSMutableSet*)recordsSet;




@property (nonatomic, strong) NSOrderedSet *resources;

- (NSMutableOrderedSet*)resourcesSet;





@end

@interface _Nodes (CoreDataGeneratedAccessors)

- (void)addChildren:(NSSet*)value_;
- (void)removeChildren:(NSSet*)value_;
- (void)addChildrenObject:(Nodes*)value_;
- (void)removeChildrenObject:(Nodes*)value_;

- (void)addContent:(NSSet*)value_;
- (void)removeContent:(NSSet*)value_;
- (void)addContentObject:(Content*)value_;
- (void)removeContentObject:(Content*)value_;

- (void)addRecords:(NSSet*)value_;
- (void)removeRecords:(NSSet*)value_;
- (void)addRecordsObject:(Record*)value_;
- (void)removeRecordsObject:(Record*)value_;

- (void)addResources:(NSOrderedSet*)value_;
- (void)removeResources:(NSOrderedSet*)value_;
- (void)addResourcesObject:(Resources*)value_;
- (void)removeResourcesObject:(Resources*)value_;

@end

@interface _Nodes (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSString*)value;




- (NSNumber*)primitiveItem_id;
- (void)setPrimitiveItem_id:(NSNumber*)value;

- (int16_t)primitiveItem_idValue;
- (void)setPrimitiveItem_idValue:(int16_t)value_;




- (NSNumber*)primitiveNode_id;
- (void)setPrimitiveNode_id:(NSNumber*)value;

- (int16_t)primitiveNode_idValue;
- (void)setPrimitiveNode_idValue:(int16_t)value_;




- (NSNumber*)primitivePage_id;
- (void)setPrimitivePage_id:(NSNumber*)value;

- (int16_t)primitivePage_idValue;
- (void)setPrimitivePage_idValue:(int16_t)value_;




- (NSNumber*)primitiveParent;
- (void)setPrimitiveParent:(NSNumber*)value;

- (int64_t)primitiveParentValue;
- (void)setPrimitiveParentValue:(int64_t)value_;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSNumber*)primitiveRecord_id;
- (void)setPrimitiveRecord_id:(NSNumber*)value;

- (int16_t)primitiveRecord_idValue;
- (void)setPrimitiveRecord_idValue:(int16_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveType;
- (void)setPrimitiveType:(NSString*)value;





- (NSMutableSet*)primitiveChildren;
- (void)setPrimitiveChildren:(NSMutableSet*)value;



- (NSMutableSet*)primitiveContent;
- (void)setPrimitiveContent:(NSMutableSet*)value;



- (Nodes*)primitiveParent_id;
- (void)setPrimitiveParent_id:(Nodes*)value;



- (NSMutableSet*)primitiveRecords;
- (void)setPrimitiveRecords:(NSMutableSet*)value;



- (NSMutableOrderedSet*)primitiveResources;
- (void)setPrimitiveResources:(NSMutableOrderedSet*)value;


@end
