// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Device.h instead.

#import <CoreData/CoreData.h>


extern const struct DeviceAttributes {
	__unsafe_unretained NSString *defaultPresentation;
	__unsafe_unretained NSString *identifier;
	__unsafe_unretained NSString *idleScreenActive;
	__unsafe_unretained NSString *idleScreenTitle;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *resetTime;
} DeviceAttributes;

extern const struct DeviceRelationships {
	__unsafe_unretained NSString *presentations;
	__unsafe_unretained NSString *resources;
} DeviceRelationships;

extern const struct DeviceFetchedProperties {
} DeviceFetchedProperties;

@class Presentation;
@class Resources;








@interface DeviceID : NSManagedObjectID {}
@end

@interface _Device : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (DeviceID*)objectID;





@property (nonatomic, strong) NSNumber* defaultPresentation;



@property int16_t defaultPresentationValue;
- (int16_t)defaultPresentationValue;
- (void)setDefaultPresentationValue:(int16_t)value_;

//- (BOOL)validateDefaultPresentation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* identifier;



//- (BOOL)validateIdentifier:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* idleScreenActive;



@property BOOL idleScreenActiveValue;
- (BOOL)idleScreenActiveValue;
- (void)setIdleScreenActiveValue:(BOOL)value_;

//- (BOOL)validateIdleScreenActive:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* idleScreenTitle;



//- (BOOL)validateIdleScreenTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* resetTime;



@property int16_t resetTimeValue;
- (int16_t)resetTimeValue;
- (void)setResetTimeValue:(int16_t)value_;

//- (BOOL)validateResetTime:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *presentations;

- (NSMutableSet*)presentationsSet;




@property (nonatomic, strong) NSSet *resources;

- (NSMutableSet*)resourcesSet;





@end

@interface _Device (CoreDataGeneratedAccessors)

- (void)addPresentations:(NSSet*)value_;
- (void)removePresentations:(NSSet*)value_;
- (void)addPresentationsObject:(Presentation*)value_;
- (void)removePresentationsObject:(Presentation*)value_;

- (void)addResources:(NSSet*)value_;
- (void)removeResources:(NSSet*)value_;
- (void)addResourcesObject:(Resources*)value_;
- (void)removeResourcesObject:(Resources*)value_;

@end

@interface _Device (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveDefaultPresentation;
- (void)setPrimitiveDefaultPresentation:(NSNumber*)value;

- (int16_t)primitiveDefaultPresentationValue;
- (void)setPrimitiveDefaultPresentationValue:(int16_t)value_;




- (NSString*)primitiveIdentifier;
- (void)setPrimitiveIdentifier:(NSString*)value;




- (NSNumber*)primitiveIdleScreenActive;
- (void)setPrimitiveIdleScreenActive:(NSNumber*)value;

- (BOOL)primitiveIdleScreenActiveValue;
- (void)setPrimitiveIdleScreenActiveValue:(BOOL)value_;




- (NSString*)primitiveIdleScreenTitle;
- (void)setPrimitiveIdleScreenTitle:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveResetTime;
- (void)setPrimitiveResetTime:(NSNumber*)value;

- (int16_t)primitiveResetTimeValue;
- (void)setPrimitiveResetTimeValue:(int16_t)value_;





- (NSMutableSet*)primitivePresentations;
- (void)setPrimitivePresentations:(NSMutableSet*)value;



- (NSMutableSet*)primitiveResources;
- (void)setPrimitiveResources:(NSMutableSet*)value;


@end
