// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Links.h instead.

#import <CoreData/CoreData.h>


extern const struct LinksAttributes {
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *url;
} LinksAttributes;

extern const struct LinksRelationships {
	__unsafe_unretained NSString *content;
} LinksRelationships;

extern const struct LinksFetchedProperties {
} LinksFetchedProperties;

@class Content;




@interface LinksID : NSManagedObjectID {}
@end

@interface _Links : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (LinksID*)objectID;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Content *content;

//- (BOOL)validateContent:(id*)value_ error:(NSError**)error_;





@end

@interface _Links (CoreDataGeneratedAccessors)

@end

@interface _Links (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (Content*)primitiveContent;
- (void)setPrimitiveContent:(Content*)value;


@end
