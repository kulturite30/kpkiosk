#import "_Files.h"
#import "Nodes.h"

@interface Files : _Files {}
// Custom logic goes here.
-(UIImage*) thumbnailWithSize:(CGSize*) size;
@end
