// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Setting.m instead.

#import "_Setting.h"

const struct SettingAttributes SettingAttributes = {
	.identifier = @"identifier",
	.value = @"value",
};

const struct SettingRelationships SettingRelationships = {
};

const struct SettingFetchedProperties SettingFetchedProperties = {
};

@implementation SettingID
@end

@implementation _Setting

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Setting" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Setting";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Setting" inManagedObjectContext:moc_];
}

- (SettingID*)objectID {
	return (SettingID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic identifier;






@dynamic value;











@end
