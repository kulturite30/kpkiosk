//
//  KKModalSegue.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 29.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKCustomModalSegue : UIStoryboardSegue

@end
