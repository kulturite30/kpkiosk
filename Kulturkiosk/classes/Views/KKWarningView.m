//
//  KKWarningView.m
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 18.01.13.
//
//

#import "KKWarningView.h"
#import "KKLanguage.h"

@implementation KKWarningView

-(void)awakeFromNib
{
 
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    
  }
  return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  [super touchesBegan:touches withEvent:event];
  [[NSNotificationCenter defaultCenter] postNotificationName:UserDidTouchWindowNotification object:nil userInfo:nil];
  [self removeFromSuperview];
}

@end
