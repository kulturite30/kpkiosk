//
//  KKCardLayout.m
//  Kulturkiosk
//
//  Created by Marcus Ramberg on 27.11.13.
//
//

#import "KKCardLayout.h"

@implementation KKCardLayout

- (CGSize)collectionViewContentSize
{
  NSInteger itemCount = [self.collectionView numberOfItemsInSection:0];
  NSInteger pages = ceil((float)itemCount / 6.0);
  
  return CGSizeMake(self.collectionView.frame.size.width * pages, self.collectionView.frame.size.height);
}

@end
