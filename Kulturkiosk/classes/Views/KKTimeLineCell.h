//
//  KKTimeLineCell.h
//  Kulturkiosk
//
//  Created by Rune Botten on 06.09.12.
//
//

#import <UIKit/UIKit.h>
#import "KKAnnotationView.h"

@interface KKTimeLineCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *highlightImageView;
@property (weak, nonatomic) IBOutlet KKAnnotationView *annotationView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
