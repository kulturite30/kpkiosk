//
//  KKZoomingImageView.m
//  Kulturkiosk
//
//  Created by Rune Botten on 11.12.12.
//
//

#import "KKZoomingImageView.h"

@interface KKZoomingImageView() <UIScrollViewDelegate>
@property (strong, nonatomic) UIImageView *imageView;
@end

@implementation KKZoomingImageView

-(void) setup
{
  [self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
  [self addObserver:self forKeyPath:@"bounds" options:NSKeyValueObservingOptionNew context:nil];
  [self addObserver:self forKeyPath:@"zoomEnabled" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
  
  self.backgroundColor = [UIColor clearColor];
  
  self.userInteractionEnabled = YES;
  self.delegate = self;
  self.autoresizesSubviews = YES;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self.superview touchesBegan:touches withEvent:event];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self.superview touchesEnded:touches withEvent:event];
}

-(void) dealloc
{
  [self removeObserver:self forKeyPath:@"image"];
  [self removeObserver:self forKeyPath:@"bounds"];
  [self removeObserver:self forKeyPath:@"zoomEnabled"];
  
  self.image = nil;
  self.imageView = nil;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setup];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self setup];
  }
  return self;
}

-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
  if([keyPath isEqualToString:@"image"]) {
    
    if(self.imageView)
      [self.imageView removeFromSuperview];
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = self.image;
    [self addSubview:self.imageView];
    
  } else if ([keyPath isEqualToString:@"bounds"]) {
    if(self.imageView)
      self.imageView.bounds = self.bounds;
  } else if ([keyPath isEqualToString:@"zoomEnabled"]) {
    
    BOOL old = [[change valueForKey:@"old"] boolValue];
    BOOL new = [[change valueForKey:@"new"] boolValue];
    
    // Change?
    if(new != old) {
      
      if(new) {
        
        self.zoomScale = 1.0;
        self.maximumZoomScale = 5;
        self.minimumZoomScale = 1.0;
        
        self.imageView.multipleTouchEnabled = YES;
        
        NSLog(@"This cell now allows zooming");
      } else {
        self.zoomScale = 1.0;
        self.maximumZoomScale = 1.0;
        self.minimumZoomScale = 1.0;

        NSLog(@"This cell now DISALLOWS zooming");
        self.imageView.multipleTouchEnabled = NO;
      }
    }
  }
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  // center the image as it becomes smaller than the size of the screen
  CGSize boundsSize = self.bounds.size;
  CGRect frameToCenter = self.imageView.frame;
  
  // center horizontally
  if (frameToCenter.size.width < boundsSize.width)
    frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
  else
    frameToCenter.origin.x = 0;
  
  // center vertically
  if (frameToCenter.size.height < boundsSize.height)
    frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
  else
    frameToCenter.origin.y = 0;
  
  self.imageView.frame = frameToCenter;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
  return YES;
}

- (void)configureForImageSize:(CGSize)imageSize
{
  self.zoomScale = 1.0;
  CGSize boundsSize = [self bounds].size;
  
  // set up our content size and min/max zoomscale
  CGFloat xScale = boundsSize.width / imageSize.width;    // the scale needed to perfectly fit the image width-wise
  CGFloat yScale = boundsSize.height / imageSize.height;  // the scale needed to perfectly fit the image height-wise
  CGFloat minScale = MIN(xScale, yScale);                 // use minimum of these to allow the image to become fully visible
  
  // on high resolution screens we have double the pixel density, so we will be seeing every pixel if we limit the
  // maximum zoom scale to 0.5.
  // CGFloat maxScale = 1.0 / [[UIScreen mainScreen] scale];
  
  // don't let minScale exceed maxScale. (If the image is smaller than the screen, we don't want to force it to be zoomed.)
  //  if (minScale > maxScale) {
  //    minScale = maxScale;
  //  }
  
  self.contentSize = imageSize;
  self.maximumZoomScale = 2.5;
  self.minimumZoomScale = 0.2;
  self.zoomScale = minScale;  // start out with the content fully visible
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
  return self.imageView;
}

@end
