//
//  KKHeaderCollectionView.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//
//

#import "KKHeaderCollectionView.h"

@implementation KKHeaderCollectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
@synthesize title;
@synthesize text;
@synthesize featureImage;
@synthesize node = _node;
@synthesize readMore;

@end
