//
//  KKTimeLineView.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 30.05.12.
//  Copyright (c) 2012 Nordaaker Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KKTimeLineView : UIScrollView <UIScrollViewDelegate>

@end
