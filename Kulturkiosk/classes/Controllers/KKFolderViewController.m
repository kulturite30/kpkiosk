//
//  KKFolderViewController.m
//  Kulturkiosk
//
//  Created by Martin Jensen on 16.07.12.
//  Copyright (c) 2012 Nordaaker Ltd.. All rights reserved.
//

#import "KKFolderViewController.h"
#import "KKTimeLineViewController.h"
#import "KKHeaderCollectionView.h"
#import "KKNodeCell.h"
#import "NSString+HTML.h"

@interface KKFolderViewController ()

@end

@implementation KKFolderViewController

@synthesize node = _node;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(didChangeLanguage:)
                                               name:@"languageChange"
                                             object:nil];
}

-(void) didChangeLanguage:(NSNotification*) notification
{
  self.childs = [[self.node childrenForLanguage:[KKLanguage currentLanguage]] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
  [self.collectionView reloadData];
}

-(void)setNode:(Nodes *)node
{
  _node = node;
  
  // Only use childs with content for the current language
  self.childs =  [[node childrenForLanguage:[KKLanguage currentLanguage]] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"position" ascending:YES]]];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKNodeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FolderViewCell" forIndexPath:indexPath];
  Nodes *child = [self.childs objectAtIndex:(NSUInteger)indexPath.row];
  
  Content *c = [child contentForLanguage:[KKLanguage currentLanguage]];
  cell.title.text = c.title;
  cell.accessoryLabel.text = [[KKLanguage localizedString:@"Read more"] uppercaseString];
  cell.accessoryLabel.font = [UIFont fontWithName:@"Myriad-Pro" size:18];
  cell.image.image = [child mainImage];
  
  return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
          viewForSupplementaryElementOfKind:(NSString *)kind
                                atIndexPath:(NSIndexPath *)indexPath
{
  KKHeaderCollectionView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"FolderHeaderView" forIndexPath:indexPath];
  

  Content *content = [self.node contentForLanguage:[KKLanguage currentLanguage]];
  
  view.text.font = [UIFont fontWithName:@"MyriadPro-Regular" size:18];
  view.title.font = [UIFont fontWithName:@"MyriadPro-Bold" size:48];
  
//  NSLog(@"%@", self.node);
  if(content.desc)
    view.text.text = [content.desc stringByConvertingHTMLToPlainText];
  else
    view.text.text = @"";
  if(content.title)
    view.title.text = [content.title stringByConvertingHTMLToPlainText];
  else
    view.title.text = @"";

  NSArray *files = [self.node imagesWithType:KKFormatHeader];
  if([files count])
  {
    Files *f = files[0];
    UIImage *image = [UIImage imageWithContentsOfFile:f.localUrl];

    view.featureImage.image = image;
    view.featureImage.contentMode = UIViewContentModeScaleAspectFill;
    view.featureImage.clipsToBounds = YES;
  } else {
    view.featureImage.image = nil;
  }

  return view;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return (NSInteger)[self.childs count];
}

#pragma mark -
#pragma mark Collection View Data Source

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  Nodes *node = [self.childs objectAtIndex:(NSUInteger)indexPath.row];
  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
  KKFolderViewController *nodeView;
  // Check type for selected item
  if (node.nodeType == KKTypeFolder) {
    nodeView = [storyBoard instantiateViewControllerWithIdentifier:@"FolderView"];
  } else if (node.nodeType == KKTypeRecord)
  {
    nodeView = [storyBoard instantiateViewControllerWithIdentifier:@"SingletViewController1"];
  }
  nodeView.node = node;
  [nodeView.view setFrame:CGRectMake(83, 0, 875, 724)];
  
  
  [self.navigationController pushViewController:nodeView animated:YES];
}

@end
