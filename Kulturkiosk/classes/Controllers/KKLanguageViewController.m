//
//  KKLanguageViewController.m
//  Kulturkiosk
//
//  Created by Rune Botten on 23.08.12.
//
//

#import "KKLanguageViewController.h"
#import "KKLanguageChoiceCell.h"
#import "KKLanguage.h"
#import <QuartzCore/QuartzCore.h>

@interface KKLanguageViewController ()

@end

@implementation KKLanguageViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lang-box-gradient.png"]];

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return [[KKLanguage availableLanguages] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
  return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
  KKLanguageChoiceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LanguageCell" forIndexPath:indexPath];
  
  NSDictionary *language = [[KKLanguage availableLanguages] objectAtIndex:indexPath.row];
  cell.tag = [[language objectForKey:@"id"] intValue];
  cell.title.text = [language valueForKey:@"title"];
  [cell.title setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:16]];
  cell.flagView.image = [language valueForKey:@"image"];
  
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  
  UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
  
  KKLanguageType newLanguage = (KKLanguageType)cell.tag;
  [KKLanguage setLanguage:newLanguage];

}

@end
