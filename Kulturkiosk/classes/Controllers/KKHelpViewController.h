//
//  KKHelpViewController.h
//  Kulturkiosk
//
//  Created by Audun Kjelstrup on 8/9/13.
//
//

#import <UIKit/UIKit.h>

@interface KKHelpViewController : UIViewController

@property (assign, nonatomic) KKHelpType helpType;

@end
