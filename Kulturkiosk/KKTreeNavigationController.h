//
//  RBViewController.h
//  dasdas
//
//  Created by Rune Botten on 09.08.12.
//  Copyright (c) 2012 Rune Botten. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KKTreeCollectionViewController.h"

@interface KKTreeNavigationController : UITableViewController <RBCollectionViewControllerDelegate>

@end
